import board
import neopixel
import time

strip = neopixel.NeoPixel(board.D18, 200, brightness=1)
global currentcolor
currentcolor = 255
def on():
    strip.brightness = 1

def off():
    strip.brightness = 0

def Brightness(brightness):
    strip.brightness = brightness/100

def RGB(color):
    newcolor = (rgbtorbg(color))
    for i in range(200):
        strip[i] = newcolor
        time.sleep(1/(i+1))
    global currentcolor
    currentcolor = newcolor


def rgbtorbg(rgbint):
    green = rgbint & 255
    blue = (rgbint >> 8) & 255
    red = (rgbint >> 16) & 255
    rgb = red
    rgb = (rgb << 8) + green
    rgb = (rgb << 8) + blue
    return rgb

def activate():
    global currentcolor
    for i in range(200):
        strip[i] = 255
        strip[i-3] = currentcolor
